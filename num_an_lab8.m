N=5;

%rng(8)

A=(rand(N))*10;

A=my_hess(A);
[lambda,V0]=my_QReig(A);
V=my_eigenvectors(lambda,A);

disp('original matrix')
disp(A)

disp('mine')
disp(real(V*diag(lambda)*inv(V)));
[V1, D] = eigs(A);
disp('by matlab')
disp(real(V1*D*inv(V1)))

disp('eigenvalues found');
disp(lambda);
disp('eigenvector found')
disp(V);

disp('eigenvalues by matlab');
disp(diag(D));
disp('eigenvector by matlab')
disp(V1);

disp('qr eigenvectors')
disp(V0)
disp('qr-restored matrix')
disp(real(V0*diag(lambda)*inv(V0)))

function V = my_eigenvectors(lambda,A)
    N=length(lambda);
    V=zeros(N);
    eps=0.01;
    for i=1:N
        x=rand(N,1);
        x=x/norm(x);
        err=1;
        k=1;
        while err>eps || k<1000
            x=(A-lambda(i)*eye(N))\x;
            x=x/norm(x);
            err=norm(A*x-lambda(i)*x);
            k=k+1;
        end
        V(:,i)=x;
    end
end

function [lambda,V] = my_QReig(A)
    eps = 0.0001;
    N=length(A);
    V = eye(N);
    for i=1:100
        [Q, R] = my_QR(A);
        A=R*Q;
        V=V*Q;
    end

    lambda = zeros(N,1);

    i=1;
    while i<N
        if abs(A(i+1,i))<eps
            lambda(i)=A(i,i);
        else
            a=A(i,i); b=A(i,i+1);
            c=A(i+1,i); d=A(i+1,i+1);
            dis = sqrt(a^2-2*a*d+4*b*c+d^2);
            lambda(i)=(a+d+dis)/2;
            lambda(i+1)=conj(lambda(i));
            i=i+1;
        end
        i=i+1;
    end
    if A(N,N-1)<eps
        lambda(N)=A(N,N);
    end
end

function [Q, R] = my_QR(A)
    N=length(A);
    Q=diag(ones(N,1));
    for i=1:N-1
        x=A(i:end,i);
        H=my_haus(x);
        U=blkdiag(diag(ones(i-1,1)),H);
        A=U*A;
        Q=Q*U;
    end
    R=A;
end

function A_h = my_hess(A)
    N=length(A);
    for i=1:N-2
        x=A(i+1:end,i);
        H=my_haus(x);
        U=blkdiag(diag(ones(i,1)),H);
        A=U*A*U;
    end
    A_h=A;
end

function H = my_haus(x)
        N=length(x);
        e_i=zeros(N,1);
        e_i(1)=1;
        v=x+norm(x)*e_i;
        H=diag(ones(N,1))-2*v*v'/norm(v)^2;
end

